const path = require("path");

module.exports = {
  entry: "./src/index.js",
  output: {
    filename: "password-policy-guard.js",
    path: path.resolve(__dirname, "dist"),
  },
  devServer: {
    contentBase: "./dist",
  },
};
