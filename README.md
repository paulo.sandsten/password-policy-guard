# Password Policy Guard

> Note: This package is not published yet! Installation must be by manually downloading and building the distrubution code with the build script.

A library that will check your password policies for you because we just need some awesomeness in out dev lives.

The idea of this library actually grew out of an Friday early morning interview that really stuck with me and pretty much grew to an obsession during the weekend. That's when I decided to redo the challenge into a working library on a Sunday.

The basic idea was that a user of the function, in this case the library, would supply a JSON with the password policy that we import in or fetch. I found it interesting as it was really a deeper question as it was also about standardizing a contract between multiple applications.

I started researching what currently exist, and most of the libraries use functions such as if the string provided has an uppercase letter or if it includes a number or or two, and so on.

They are all really good solutions for one application, but what if we need multiple applications that share the same policy? That's where this library comes in: to have one single source of truth for your password policy in a shared JSON file.

## Install

```bash
npm i password-policy-checker
```

## The policy contract (schema)

Before even using the library, we need to understand that it needs a policy supplied to it. To make this a universal implementation, we need to standardize how that contract would look like. Here we have a minimal example for a global policy that the password will be tested against:

Example of a global policy:

```json
{
  "global": {
    "test": "^(?=.*[a-z])(?=(.*[A-Z]){1,})(?=(.*\\d){1,})(?=(.*[!@#$%^&?*()]){1,})[A-Za-z\\d!@#$%^&?*()]{8,}$"
  }
}
```

You could also divide the policy into smaller "conditions": then we test every character in the password against it. Don't worry, we only iterate through the password once for all the conditions and not once per condition.

```json
{
  "minimum": {
    "test": "isAllowed",
    "length": 8,
    "comparisonOperator": "gte"
  }
}
```

### Policy object

| Property      | Type               | Description                                                                          |
| ------------- | ------------------ | ------------------------------------------------------------------------------------ |
| global        | \<GlobalCondition> | The properties in here are for when a password will be tested against a global regex |
| \<customName> | \<Condition>       | A custom condition.                                                                  |

> Note: If you have a global policy and custom conditions, the library will always use the global policy as it has higher precendence.

### Global Condition object

| Property                | Type   | Description                                                                                                 | Example                                                      |
| ----------------------- | ------ | ----------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------ |
| name (optional)         | string | Your own naming convention for the policy. For example, "My global password policy"                         | "My company password policy"                                 |
| description (optional)  | string | Your own description for the policy. It's recommended to be used for your application as for developers     | "You will need 8 characters minimum"                         |
| errorMessage (optional) | string | Your error message                                                                                          | "Seems like the supplied password is less than 8 characters" |
| test (required)         | string | A regex string to test the password against. The format is to put the regex in quotes. For example, "[a-z]" | "[A-Za-z\\\d!@#$%^&?*()]{8,}$"                               |

> OK. Regex experts will look at this and be like, "Dude, this says it's 8 consecutive characters". To you, add an issue and make the PR for any changes :).

> Note that when we supply the regex in a JSON, we need to make sure we are escaping the string where needed. Easiest is to validate your json (regex) in an validator. For example, [JSON Formatter](https://jsonformatter.curiousconcept.com/#)

### Condition object

| Property                      | Type   | Description                                                                                  | Example                                                      |
| ----------------------------- | ------ | -------------------------------------------------------------------------------------------- | ------------------------------------------------------------ |
| name (optional)               | string | Your naming of the policy                                                                    | "Minimum length of characters"                               |
| description (optional)        | string | Your description of the policy                                                               | "You will need 8 characters minimum"                         |
| errorMessage (optional)       | string | Your error message                                                                           | "Seems like the supplied password is less than 8 characters" |
| test (required)               | string | Supply a regex string that does a check, or the name of a one of our library policy funcions | "[a-z]" or "isUpperCase"                                     |
| length (required)             | number | Give the length that the comparison will check against                                       | 2                                                            |
| comparisonOperator (required) | string | Supply the comparison operator to check against                                              | ">=" or "gte"                                                |

## Policy schema example

```json
{
  "minimum": [
    {
      "test": "[A-Za-z\\d!@#$%^&?*()]$",
      "length": 8,
      "comparisonOperator": "gte"
    }
  ]
}
```

## Using the library

Let's assume you have the policy schema like the example above:

```javascript
import ppc from "password-policy-checker";
// A bundler or loader will make the policy into a normal JavaScript object when importing it.
// If you don't use that, you will need to parse it first.
import policy from "./your-policy.json";
const schema = ppc.generateSchema(policy);

const password = "testingThisStringAsAPassword";
if (schema.validate(password)) {
  console.log("Valid password");
}
```

## Library functions out-of the box

If we don't want to supply our own regex to the condition test, you can use an in-built method.

| Method    | Description                                                                                                                                                   |
| --------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| isAllowed | Used typically for minimum and maximum checks. Will check if the string character is allowed, which is set by the default regex check: [A-Za-z0-9!@#$%^&*()]. |
| isLower   | Checks to see if the string character is a lower case characters.                                                                                             |
| isUpper   | Checks to see if the string character is a upper case characters.                                                                                             |
| isDigit   | Checks to see if the string character is a digit.                                                                                                             |
| isSpecial | Checks to see if the string character is a special character, which is set by the default regex check: [!@#$%^&*()].                                          |

## Comparison operators

Every condition needs to be supplied with what comparison operator we are checking the password with. For example, are we checking if the test is suppose to be greater than and equal, or do we need it to be an exact length?

Internally, this gets mapped to a function (x = number of matching string characters, y = length for the condition).

| comparisonOperator | Function being returned |
| ------------------ | ----------------------- |
| ">" or "gt"        | (x, y) => x > y         |
| ">=" or "gte"      | (x, y) => x >= y        |
| "<" or "lt"        | (x, y) => x < y         |
| "<=" or "lte"      | (x, y) => x <= y        |
| "==" or "e"        | (x, y) => x == y        |
| "!=" or "ne"       | (x, y) => x != y        |
| "===" or "te"      | (x, y) => x === y       |
| "!==" or "nte"     | (x, y) => x !== y       |
