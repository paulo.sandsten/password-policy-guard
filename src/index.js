import { cloneDeep } from "lodash";
import {
  isArray,
  isJSON,
  isObject,
  comparisonOperator,
  toRegexTest,
} from "./utils";
import errorMessages from "./config/errorMessages.json";
import defaultPolicy from "./config/defaultPolicy.json";
import defaultConfig from "./config/defaultConfig.json";

const passwordPolicyGuard = () => {
  let _source = {};
  let _conditions = {};

  const _isLower = (c) => c === c.toLowerCase();
  const _isUpper = (c) => c === c.toUpperCase();
  const _isDigit = (c) => !Number.isNaN(c);
  const _isSpecial = (c) =>
    toRegexTest(c, defaultConfig.allowedSpecialCharacters);
  const _isAllowed = (c) => toRegexTest(c, defaultConfig.allowedCharacters);

  let _libraryMethods = {
    isAllowed: _isAllowed,
    isLower: _isLower,
    isUpper: _isUpper,
    isDigit: _isDigit,
    isSpecial: _isSpecial,
  };

  const _conditionHitDecorator = (passwordStringChar, conditions) => {
    Object.keys(conditions).forEach((c) => {
      let test = false;
      conditions[c].hits = conditions[c].hits || 0;

      if (_libraryMethods.hasOwnProperty(conditions[c].test)) {
        test = !!conditions[c].test(password);
      }

      try {
        test = toRegexTest(passwordStringChar, conditions[c].test);
      } catch (err) {
        console.error(err);
        return;
      }

      if (test) {
        conditions[c].hits += 1;
      }

      return;
    });

    return;
  };

  /**
   * @param {*} password
   * @returns {Boolean}
   */
  const validate = (password) => {
    try {
      if (_conditions.global?.test) {
        try {
          if (!_conditions.global.test instanceof RegExp) {
            throw new SyntaxError(errorMessages.invalidRegex);
          }
          return toRegexTest(password, _conditions.global.test);
        } catch (err) {
          console.error(err);
          return undefined;
        }
      } else if (_conditions.global) {
        throw new SyntaxError(errorMessages.missingGlobalTestProperty);
      }
    } catch (err) {
      console.error(err);
      return err;
    }

    const p = password.split("");
    _conditions = { ...cloneDeep(_source) };
    p.forEach((s) => {
      _conditionHitDecorator(s, _conditions);
    });

    let _checksMissed = 0;
    Object.keys(_conditions).forEach((c) => {
      if (
        !comparisonOperator[_conditions[c].comparisonOperator](
          _conditions[c].hits,
          _conditions[c].length
        )
      ) {
        _checksMissed += 1;
      }
    });

    return !_checksMissed;
  };

  /**
   * @param {*} policy
   * @returns {Object}
   */
  const generateSchema = (policy = defaultPolicy) => {
    try {
      if (!isObject(policy)) {
        throw new TypeError(errorMessages.invalidObject);
      }
      _source = { ...cloneDeep(policy) };
      _conditions = { ...cloneDeep(policy) };

      return {
        validate,
        conditions: _conditions,
      };
    } catch (err) {
      console.error(err);
      return undefined;
    }
  };

  return { generateSchema };
};

export default passwordPolicyGuard();
