import passwordPolicyGuard from "../index";
import customConditionsPolicy from "./mocks/policyWithCustomConditions.json";
import libraryConditionsPolicy from "./mocks/policyWithOnlyLibraryConditions.json";
const expectedSchema = { ...customConditionsPolicy };

test("Checks for the schema to be generated and returned", () => {
  const schema = passwordPolicyGuard.generateSchema(customConditionsPolicy);

  expect(schema.conditions).toEqual(expectedSchema);
});

test("Checks for the custom condition with 3 special chars: ! @ # $ & *", () => {
  const schema = passwordPolicyGuard.generateSchema(customConditionsPolicy);

  expect(schema.validate("@Hello1")).toEqual(false);
  expect(schema.validate("@!#Hello1")).toEqual(true);
});
