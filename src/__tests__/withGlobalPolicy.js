import passwordPolicyGuard from "../index";
import defaultPolicy from "../config/defaultPolicy.json";
const expectedSchema = { ...defaultPolicy };

test("Checks for the schema to be generated and returned", () => {
  const schema = passwordPolicyGuard.generateSchema(defaultPolicy);

  expect(schema.conditions).toEqual(expectedSchema);
});

test("Checks for the schema to be generated and returned with a default policy when none supplied", () => {
  const schema = passwordPolicyGuard.generateSchema();

  expect(schema.conditions).toEqual(expectedSchema);
});

test("Checks the validation for minimum length, which is 8.", () => {
  const schema = passwordPolicyGuard.generateSchema(defaultPolicy);

  expect(schema.validate("@Hello1")).toEqual(false);
  expect(schema.validate("@Hello1!")).toEqual(true);
});

test("Checks the validation for at least one upper case letter.", () => {
  const schema = passwordPolicyGuard.generateSchema(defaultPolicy);

  expect(schema.validate("@hello1!")).toEqual(false);
  expect(schema.validate("@Hello1!")).toEqual(true);
});

test("Checks the validation for at least one digit.", () => {
  const schema = passwordPolicyGuard.generateSchema(defaultPolicy);

  expect(schema.validate("@Hello!!")).toEqual(false);
  expect(schema.validate("@Hello1!")).toEqual(true);
});

test("Checks the validation for at least one special character.", () => {
  const schema = passwordPolicyGuard.generateSchema(defaultPolicy);

  expect(schema.validate("1hello11")).toEqual(false);
  expect(schema.validate("@Hello11")).toEqual(true);
});

test("Checks the validation for at least one special character.", () => {
  const schema = passwordPolicyGuard.generateSchema(defaultPolicy);

  expect(schema.validate("1hello11")).toEqual(false);
  expect(schema.validate("@Hello11")).toEqual(true);
});
