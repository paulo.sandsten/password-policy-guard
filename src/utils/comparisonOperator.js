export const comparisonOperator = {
  ">": (x, y) => x > y,
  ">=": (x, y) => x >= y,
  "<": (x, y) => x < y,
  "<=": (x, y) => x <= y,
  "==": (x, y) => x == y,
  "!=": (x, y) => x !== y,
  "===": (x, y) => x === y,
  "!==": (x, y) => x !== y,
  gt: function (x, y) {
    return x > y;
  },
  gte: (x, y) => x >= y,
  lt: (x, y) => x < y,
  lte: (x, y) => x <= y,
  e: (x, y) => x == y,
  ne: (x, y) => x !== y,
  te: (x, y) => x === y,
  nte: (x, y) => x !== y,
};
