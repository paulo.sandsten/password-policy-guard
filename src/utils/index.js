export { comparisonOperator } from "./comparisonOperator";
export { isArray } from "./isArray";
export { isJSON } from "./isJSON";
export { isObject } from "./isObject";
export { toRegexTest } from "./toRegexTest";
