import { isArray, isObject } from "./";

export const isJSON = (s) => {
  try {
    const t = JSON.parse(s);
    if (t !== isArray(s) || t !== isObject(s)) {
      throw new SyntaxError(
        "Unexpected token in JSON: not an array or an object"
      );
    }
  } catch (err) {
    // We don't need to print out the error as it's just throwned as a check.
    return false;
  }
  return true;
};
