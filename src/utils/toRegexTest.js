export const toRegexTest = (s, e) => {
  const regex = new RegExp(e);
  return !!regex.test(s);
};
