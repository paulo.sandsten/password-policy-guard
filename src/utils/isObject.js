export const isObject = (s) =>
  Object.prototype.toString.call(s) === "[object Object]";
